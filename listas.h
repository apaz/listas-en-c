#ifndef LISTAS_H
#define LISTAS_H

#include <stdlib.h>

struct node {
  int data;
  struct node* next;
};

struct node* BuildOneTwoThree();
struct node* Length(struct node* head);

#endif
